//problem: user when clicking on image goes to a dead end
//solution: create an overlay with the large image - Lightbox

var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");
var $caption = $("<p></p>");

// An image to overlay
$overlay.append($image);

// a caption to overlay
$overlay.append($caption);

// add overlay    
$("body").append($overlay);

// capture click event on a link to an image
$("#imageGallery a").click(function(event){
  event.preventDefault();
    var href = $(this).attr("href");
    // update overlay with the image linked in the link
    $image.attr("src", href);  
  
    // show overlay
    $overlay.show(); 

    // get child's alt attribute and set caption
    var captionText = $(this).children("img").attr("alt");
    $caption.text(captionText);
});

// when overlay is clicked
$overlay.click(function(){
    // hide the overlay
    $(this).hide();
  
});
    
